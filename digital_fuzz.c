/* to compile:
   gcc -lncurses digital_fuzz.c -o digital_fuzz */

#include <ncurses.h>
#include <time.h>
#include <stdlib.h>

/* Function Prototypes */
void initialize_grid (int *);
void print_row (int *);
void reformat_line (int *);

/* Used for nanosleep. */
static struct timespec time_wait = { (time_t) 0, 100000000L};

/* Used to check for screen resizing. */
int curr_lines;
int curr_cols;

int main () {
	/* random num stuff */
	srand (time (NULL));
	
	/* start curses mode and enable non-blocking for getch. */
	nodelay (initscr (), TRUE);
	

	/* Enables the bold attribute. */
	attron (A_BOLD);

	/* Enables green color if available. */
	if (has_colors () == TRUE) {
		start_color ();
		init_pair (1, COLOR_GREEN, COLOR_BLACK);
		attron (COLOR_PAIR (1));
	}

/* This label is used in the case that the screen is resized.
   This way, along with curr_lines and curr_cols,
   a segfault won't occur. It's a bit hacky, but it works. */
initialize:
	/* Initialize the grid */
	curr_lines = LINES;
	curr_cols = COLS;
    int grid [curr_lines * curr_cols];
	initialize_grid (grid);

	/* Position for where to start each print!
	   Mod it by lines for wrap */
	int start_line = 0;

	while (getch () == ERR) {
		/* Print out each row */
		for (int i = 0; i < curr_lines; i++) {
			/* Fancy math to calculate the current place in the array. */
			print_row (grid + ((i + start_line) * curr_cols) % curr_lines);

			/* Increments to next line.
			   mods by curr_lines in case it needs to wrap.*/
			start_line = (start_line + 1) % curr_lines;
		}
		/* Sleep here and refresh () */
		nanosleep (& time_wait, NULL);
		refresh ();

		/* Increment by one to get to the lines we need to change
		   then change that line.
		   Increment by one again to start on the next line. */
		start_line = (start_line + 1) % curr_lines;
		reformat_line (grid + start_line * curr_cols);
		start_line = (start_line + 1) % curr_lines;

		/* Move cursor to beginning here. */
		move (0,0);

		/* If the size of the window has changed,
		   re-initialize and start over again. */
		if (curr_lines != LINES || curr_cols != COLS)
			goto initialize;
	}

	/* pause program for testage */
	endwin ();
	return 0;
	
}

/* Fills the grid with 0's and 1's. */
void initialize_grid (int * grid) {
	for (int i = 0; i < LINES * COLS; i++) {
		/* Get 0 or 1 */
		* grid = rand () % 2;
		/* Increment the address */
		grid += 1;
	}
}

/* generates a "random" num and either prints the number
   or a space.*/
void print_row (int * line) {
	for (int i = 0; i < curr_cols; i++) {
		if (rand () % 8 == 0) 
			printw ("%d", * line);
		else
			printw (" ");
		line += 1;
	}
}

/* takes one line (row) and gives it new "random" values */
void reformat_line (int * line) {
	for (int i = 0; i < COLS; i++) {
		* line = rand () % 2;
		line += 1;
	}
}
