/* Test circle fill algorithm. */
#include <stdio.h>
#include <ncurses.h>

void draw_circle_fill (int, int, int);
void draw_circle (int, int, int);
void draw_pixel (int, int);
void refresh_and_wait ();
void draw_rectangle (int, int, int, int);

int main () {
	initscr ();
	clear ();
	noecho ();

	draw_circle_fill (30, 10, 8);
	refresh ();

	draw_rectangle (60, 10, 5, 5);
	refresh ();

	getch ();
	endwin ();

	return 0;
}

/* x0 = 10, y0 = 20, radius = 5 */
void draw_circle (int x0, int y0, int radius) {
	int x = radius;
	int y = 0; /* 1 */
	int decision_over_2 = 1 - x;

	while (y <= x) {
		draw_pixel ( x + x0,  y + y0); /* 15, 21 up-right */
		draw_pixel ( y + x0,  x + y0); /* 11, 25 right-up */
		draw_pixel (-x + x0,  y + y0); /* 5, 21 up-left */
		draw_pixel (-y + x0,  x + y0); /* 9, 25 left-up */
		draw_pixel (-x + x0, -y + y0); /* 5, 19 down-left */
		draw_pixel (-y + x0, -x + y0); /* 9, 15 left-down */
		draw_pixel ( x + x0, -y + y0); /* 15, 19 down-right */
		draw_pixel ( y + x0, -x + y0); /* 11, 15 right-down */
		y++;

		if (decision_over_2 <= 0) {
			decision_over_2 += 2 * y + 1;

		} else {
			x--;
			decision_over_2 += 2 * (y - x) + 1;
		}
	}
}

void draw_line (int x1, int y1, int x2, int y2) {
	/* Need to make this so that it takes into account the highest. */
	if (0 == x2 - x1) {
		int y;
		for (y = y1; y <= y2; y++) {
			draw_pixel (x1, y);
		}
	} else if (0 == y2 - y1) {
		int x;
		for (x = x1; x <= x2; x++) {
			draw_pixel (x, y1);
		}
	}
}

void draw_circle_fill (int x0, int y0, int radius) {
	int x = radius;
	int y = 0;
	int decision_over_2 = 1 - x;

	while (y <= x) {
		draw_line (-x + x0, y + y0, x + x0, y + y0); /* inner up */
		draw_line (-x + x0, -y + y0, x + x0, -y + y0); /* inner down */
		draw_line (x + x0, -y + y0, x + x0, y + y0); /* outer right */
		draw_line (-x + x0, -y + y0, -x + x0, y + y0); /* outer left */
		draw_line (-y + x0, x + y0, y + x0, x + y0); /* outer up */
		draw_line (-y + x0, -x + y0, y + x0, -x + y0); /* outer down */
		draw_line (y + x0, -x + y0, y + x0, x + y0); /* inner right */
		draw_line (-y + x0, -x + y0, -y + x0, x + y0); /* inner left */

		y++;

		if (decision_over_2 <= 0) {
			decision_over_2 += 2 * y + 1;

		} else {
			x--;
			decision_over_2 += 2 * (y - x) + 1;
		}

	}

}

void draw_rectangle (int x, int y, int width, int height) {
	/* Draws a rectangle starting at position X, Y with WIDTH, HEIGHT, and RGB. */
	int i;

	/* Horizontal lines. */
	for (i = 0; i < width; i++) {
		draw_pixel (x + i, y);
		draw_pixel (x + i, y + height - 1);
	}

	/* Vertical lines. */
	for (i = 0; i < height; i++) {
		draw_pixel (x, y + i);
		draw_pixel (x + width - 1, y + i);
	}

	return;

}

void refresh_and_wait () {
	refresh ();
	sleep (.1);

	return;
}

void draw_pixel (int x, int y) {
	move (y, x);
	printw ("%c", '@');

	return;
}
